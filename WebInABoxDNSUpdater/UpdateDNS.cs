﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebInABoxDNSUpdater
{
    public class UpdateDNS
    {
        private string url;
        private string record;
        private string ttl;
        private string publicIP;

        public UpdateDNS(string url, string record, string ttl, string publicIP)
        {
            this.url = url;
            this.record = record;
            this.ttl = ttl;
            this.publicIP = publicIP;
            WebBrowser b = new WebBrowser();
            b.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted);
            b.Navigate(url);
        }

        private void b_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            WebBrowser b = sender as WebBrowser;
            string response = b.DocumentText;

            // looks in the page source to find the authenticity token.
            // could also use regular exp<b></b>ressions here.
            int index = response.IndexOf("authenticity_token");
            int startIndex = index + 41;
            string authenticityToken = response.Substring(startIndex, 44);

            // unregisters the first event handler
            // adds a second event handler
            b.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted);
            // format our data that we are going to post to the server
            // this will include our post parameters.  They do not need to be in a specific
            // order, as long as they are concatenated together using an ampersand ( & )
            string postData = string.Format("authenticity_token={0}&dns_record%5Bvalue%5D={2}&dns_record%5Bttl%5D={3}&commit=Save", authenticityToken, record, publicIP, ttl);

            ASCIIEncoding enc = new ASCIIEncoding();

            //  we are encoding the postData to a byte array
            b.Navigate(url, "", enc.GetBytes(postData), "Content-Type: application/x-www-form-urlencoded\r\n");
        }


    }
}
