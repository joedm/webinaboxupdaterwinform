﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxDNSUpdater
{
    public class DNSRecords
    {
        public string recordType { set; get; }
        public string record { set; get; }
        public string value { set; get; }
        public string url { set; get; }
    }
}
