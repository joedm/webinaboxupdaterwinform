﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebInABoxDNSUpdater
{
    public partial class Form1 : Form
    {
        static PublicIP publicIP;
        int timeriteration = 0;
        string dnsIP = "0.0.0.0";
        string ttl = "1800";
        List<DNSRecords> dnsRecords;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            richTextBoxLog.AppendText(string.Format("{0} - {1}", DateTime.Now.ToString(), "Retrieving Public IP Address"));
            publicIP = new PublicIP();
            dnsRecords = new List<DNSRecords>();
            richTextBoxLog.AppendText(string.Format("\n{0} - Your IP Address is {1}", DateTime.Now.ToString(), publicIP.IP));

            do
            {
                if (publicIP.IP != "error")
                {
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Attempting Login to webinabox.net"));
                    Login();
                }
                else
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Error obtaining Public IP address, Trying again."));
            } while (publicIP.IP == "error");

        }


        string url = "https://members.webinabox.net.au/security/login";
        string email = "caleb@liquidfusion.com.au";
        string password = "!Passw0rd";

        public void Login()
        {
            try
            {
                dnsRecords.Clear();
                WebBrowser b = new WebBrowser();
                b.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted);
                b.Navigate(url);
            }
            catch (Exception ex)
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), ex));
            }
        }

        private void b_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                WebBrowser b = sender as WebBrowser;
                string response = b.DocumentText;

                // looks in the page source to find the authenticity token.
                // could also use regular exp<b></b>ressions here.
                int index = response.IndexOf("authenticity_token");
                int startIndex = index + 41;
                string authenticityToken = response.Substring(startIndex, 40);

                // unregisters the first event handler
                // adds a second event handler
                b.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted);
                b.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted2);
                // format our data that we are going to post to the server
                // this will include our post parameters.  They do not need to be in a specific
                // order, as long as they are concatenated together using an ampersand ( & )
                string postData = string.Format("authenticity_token={2}&email=caleb%40liquidfusion.com.au&password=%21Passw0rd&commit=Log+In", email, password, authenticityToken);

                ASCIIEncoding enc = new ASCIIEncoding();

                //  we are encoding the postData to a byte array
                b.Navigate("https://members.webinabox.net.au/security/login", "", enc.GetBytes(postData), "Content-Type: application/x-www-form-urlencoded\r\n");
            }
            catch (Exception ex)
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), ex));
            }
        }

        private void b_DocumentCompleted2(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                WebBrowser b = sender as WebBrowser;
                b.Navigate("https://members.webinabox.net.au/accounts/2312/services/7146/manage-DNS");
                b.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted2);
                b.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted3);
            }
            catch (Exception ex)
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), ex));
            }
        }

        private void b_DocumentCompleted3(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            try
            {
                WebBrowser b = sender as WebBrowser;
                b.DocumentCompleted -= new WebBrowserDocumentCompletedEventHandler(b_DocumentCompleted3);

                var page = b.DocumentText;

                if (page.Contains("Logout"))
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Login Successful"));
                else
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Login Failed!"));

                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(page);

                var nodes = doc.DocumentNode.SelectNodes("//*[@id='innerRight']/div/table");

                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Retrieving list of DNS Records"));
                int i = 0;
                foreach (var node in nodes)
                {
                    i++;
                    string recordType = node.SelectSingleNode("../h2[" + i + "]").InnerText.ToString().Replace("\n", "");
                    //richTextBoxLog.AppendText(string.Format("\n    {0}", recordType));

                    var nodes2 = node.SelectNodes("./tr");
                    foreach (var n in nodes2)
                    {
                        string record = n.SelectSingleNode("./td[1]").InnerText;
                        string value = n.SelectSingleNode("./td[2]").InnerText;
                        if (record == "Record Name")
                            continue;

                        string recordurl = n.SelectSingleNode("./td[4]/a").Attributes["href"].Value;



                        if (record == "DynTracker")
                            dnsIP = value;

                        //richTextBoxLog.AppendText(string.Format("\n         {0} - {1}", record, value));
                        DNSRecords dnsRecord = new DNSRecords()
                        {
                            recordType = recordType,
                            record = record,
                            value = value,
                            url = "https://members.webinabox.net.au" + recordurl
                        };
                        dnsRecords.Add(dnsRecord);
                    }
                }
                richTextBoxLog.AppendText(string.Format("\n{0} - {1} DNS Records Found", DateTime.Now.ToString(), dnsRecords.Count()));
                richTextBoxLog.AppendText(string.Format("\n{0} - DNS DynTracker Record IP is: {1}", DateTime.Now.ToString(), dnsIP));
                if (publicIP.IP == dnsIP)
                {
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "IP Addresses Match"));
}
                else
                {
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "IP Address Has Changed"));
                    i = 0;
                    foreach (var dnsRecord in dnsRecords)
                    {
                        if (dnsRecord.value == dnsIP)
                        {
                            UpdateDNS updateDNS = new UpdateDNS(dnsRecord.url, dnsRecord.record, ttl, publicIP.IP);
                            richTextBoxLog.AppendText(string.Format("\n{0} - Updating {1}: \"{2}\"", DateTime.Now.ToString(), dnsRecord.recordType, dnsRecord.record));
                            i++;
                        }
                    }
                    richTextBoxLog.AppendText(string.Format("\n{0} - {1} Record(s) Updated", DateTime.Now.ToString(), i));
                }
                lblUpdateTime.Text = DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), ex));
            }
        }

        
        private void timerIPChecker_Tick(object sender, EventArgs e)
        {
            publicIP.refresh();

            while (publicIP.IP == "error")
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - {1}", DateTime.Now.ToString(), "Error obtaining Public IP address, Trying again."));
                publicIP.refresh();
            } 

            if (publicIP.IP != dnsIP)
            {
                richTextBoxLog.AppendText(string.Format("\n{0} - IP Address Mismatch Detected", DateTime.Now.ToString()));
                Login();                
            }
            else
            {
                lblUpdateTime.Text = DateTime.Now.ToString();
            }
            timeriteration++;

            if (timeriteration > 15)
            {
                timeriteration = 0;
                Login();
            }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

    }
}
