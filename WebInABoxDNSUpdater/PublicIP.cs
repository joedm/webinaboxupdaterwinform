﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebInABoxDNSUpdater
{
    class PublicIP
    {
        public string IP { set; get; }

        public PublicIP()
        {
            refresh();
        }

        public void refresh()
        {
            WebClient client = new WebClient();
            try
            {
                var page = client.DownloadString("http://checkip.dyndns.org/");

                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
                doc.LoadHtml(page);
                string ipScrape = doc.DocumentNode.SelectSingleNode("/html/body").InnerText.ToString();
                IP = ipScrape.Replace("Current IP Address: ", "");
            }
            catch 
            { 
                IP = "error"; 
            }
        }
    }
}
